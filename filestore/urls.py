"""filestore URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth import views as auth_views
from django.views.generic.base import TemplateView

from fs.views import registration, upload_file, home, delete_file, download_file

urlpatterns = [
    path('admin/', admin.site.urls),
]

urlpatterns += [
    path('', TemplateView.as_view(template_name='index.html'), name='index'),
    path('home/', home, name='home'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('registration/', registration, name='registration'),
    path('logout/', auth_views.LogoutView.as_view(template_name='logged_out.html'), name='logout'),
]
urlpatterns += [
    path('upload_file', upload_file, name='upload_file'),
    path('delete_file/<int:file_id>', delete_file, name='delete_file'),
    path('download_file/<int:file_id>', download_file, name='download_file'),
]
