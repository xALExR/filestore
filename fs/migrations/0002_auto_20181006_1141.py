# Generated by Django 2.1.2 on 2018-10-06 11:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fs', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userfiles',
            name='file_name',
            field=models.CharField(max_length=255),
        ),
    ]
