import os

from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.db.models import Q
from django import forms

from .forms import UserRegistrationForm
from .models import UserFiles
from .utils import generate_checksum_md5
from .storage import UniqueFileStorage
from wsgiref.util import FileWrapper
from magic import from_file

fs = UniqueFileStorage()


@login_required
def home(request):
    user_records = UserFiles.objects.filter(user_id=request.user)
    return render(request, 'home.html', {
        'file_records': user_records,
    })


def registration(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            user_data = form.cleaned_data
            username = user_data['username']
            email = user_data['email']
            password = user_data['password']
            if not (User.objects.filter(Q(username=username) | Q(email=email)).exists()):
                User.objects.create_user(username, email, password)
                user = authenticate(username=username, password=password)
                login(request, user)
                return redirect('home')
            else:
                return render(request, 'registration.html', {
                    'form': form,
                    'user_errors': ('Looks like a user with that username or email already exists.',)
                })
    else:
        form = UserRegistrationForm()
    return render(request, 'registration.html', {'form': form})


def download_file(request, file_id):
    if request.method == 'GET':
        user_file = UserFiles.objects.filter(id=file_id)[0]
        if user_file:
            file_path = fs.path(user_file.hash_file_name)
            file_wrapper = FileWrapper(open(file_path, 'rb'))
            file_mime_type = from_file(file_path, mime=True)
            response = HttpResponse(file_wrapper, content_type=file_mime_type)
            response['X-Sendfile'] = file_path
            response['Content-Length'] = os.stat(file_path).st_size
            response['Content-Disposition'] = 'attachment; filename={name}'.format(name=user_file.file_name)
            return response
        else:
            raise Http404('File does not exist.')
    return redirect('home')


@login_required
def delete_file(request, file_id):
    if request.method == 'GET':
        user_file = UserFiles.objects.filter(id=file_id)[0]
        if user_file:
            row_count = UserFiles.objects.filter(hash_file_name=user_file.hash_file_name).count()
            if row_count == 1:
                fs.delete(user_file.hash_file_name)
            user_file.delete()
            return redirect('home')
        else:
            raise Http404('File does not exist.')
    return redirect('home')


@login_required
def upload_file(request):
    if request.method == 'POST' and request.FILES.get('myfile'):
        if UserFiles.objects.filter(user_id=request.user).count() >= 100:
            messages.error(request, 'You cannot upload more than 100 files.', extra_tags='alert alert-danger')
            return redirect('home')

        file = request.FILES['myfile']
        file_type = file.name.split('.')[-1].lower()
        hash_name = generate_checksum_md5(file)

        if fs.exists(hash_name):
            existing_file = UserFiles.objects.filter(hash_file_name=hash_name)[0]
            message_template = 'This file has already been uploaded.\nFilename: {name}, downloaded by user: {user_name}'
            messages.warning(
                request,
                message_template.format(name=existing_file.file_name, user_name=existing_file.user_id.username),
                extra_tags='alert alert-warning'
            )

        fs.save(hash_name, file)
        user_file = UserFiles(
            file_name=file.name,
            user_id=request.user,
            type=file_type,
            hash_file_name=hash_name,
            size=file.size)
        user_file.save()
        return redirect('home')
    else:
        return redirect('home')
