import hashlib

BLOCKSIZE = 4096


def generate_checksum_md5(file):
    hash_md5 = hashlib.md5()
    if not hasattr(file, 'temporary_file_path'):
        for chunk in file.chunks(BLOCKSIZE):
            hash_md5.update(chunk)
    else:
        with open(file.temporary_file_path(), 'rb') as f:
            file_buffer = f.read(BLOCKSIZE)
            while len(file_buffer) > 0:
                hash_md5.update(file_buffer)
                file_buffer = f.read(BLOCKSIZE)
    return hash_md5.hexdigest()
