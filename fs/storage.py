from django.core.files.storage import FileSystemStorage


class UniqueFileStorage(FileSystemStorage):

    def save(self, name, content, max_length=None):
        if self.exists(name):
            return name
        else:
            return super().save(name, content, max_length)
