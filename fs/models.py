import datetime

from django.db import models
from django.contrib.auth.models import User


class UserFiles(models.Model):

    file_name = models.CharField(max_length=255)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    size = models.BigIntegerField(default=0)
    type = models.CharField(max_length=12)
    date_upload = models.DateTimeField(default=datetime.datetime.now)
    hash_file_name = models.CharField(max_length=32)

    class Meta:
        ordering = ['-date_upload']

    def __str__(self):
        return '{name}, size={size}, {date}'.format(
            name=self.file_name,
            size=self.size,
            date=self.date_upload
        )
