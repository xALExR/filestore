# Filestore project

This project is deduplicated file storage for many users.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

#####For development I used:

1. Python 3.6.4

2. Django 2.1.2

3. PostgreSQL 9.5.14


### Installing

At first, need to install PostgreSQL
#####Ubuntu

```
sudo apt install postgresql postgresql-contrib
```
#####Mac OS

```
brew install postgresql
```
In order to use databases different from PostgreSQL you can change DB setting in file settings.py, and don't forget to install python driver package for your DB.

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'db_name',
        'USER': 'db_user',
        'PASSWORD': 'password',
        'HOST': '',
        'PORT': '5432',
        'CONN_MAX_AGE': None,
    }
}
```

If you want to run the project in a virtual environment, install first pyenv, link on instruction: [Pyenv install](https://github.com/pyenv/pyenv#installation)

### Run project

Clone repository

```
git clone git@bitbucket.org:xALExR/filestore.git
```

Download python version 3.6.4 or newer
```
pyenv install 3.6.4
```

Create virtual environment
```
pyenv virtualenv 3.6.4 {environment_name}
```

Install dependencies
```
pyenv activate {environment_name}
pip install -r requirements.txt
```

#####Before run, please setup database config in file settings.py.
Now you can run the project
```
cd filestore
python manage.py runserver
```

## Authors

* **Rabotai Oleksandr** - (https://bitbucket.org/xALExR)
